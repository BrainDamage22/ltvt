package de.edu.ltvt.controller;

import de.edu.ltvt.dto.Question;
import de.edu.ltvt.dto.User;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A controller providing the endpoints subsequent to /displayUserQuestions .
 */
@Controller
public class DisplayUserQuestionsController {

    /**
     * The displayUserQuestionsController's user services.
     */
    private final UserService userService;

    /**
     * Creates a displayUserQuestionsController.
     * @param userService The displayUserQuestionsController's user services.
     */
    @Autowired
    public DisplayUserQuestionsController(UserService userService) {
        this.userService = userService;
    }

    /**
     * A method to get the displayUserQuestions template.
     * @param model The actual model.
     * @return The displayUserQuestions template.
     */
    @RequestMapping("displayUserQuestions")
    public String displayUserQuestions (Model model) {

        User user = userService.getCurrentUser();
        List<Question> questions = userService.getUserQuestions(user.getUsername());
        questions.sort(Comparator.comparing(Question::getDate));
        Collections.reverse(questions);
        model.addAttribute("questions", questions);
        return "displayUserQuestions";
    }

}
