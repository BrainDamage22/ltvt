package de.edu.ltvt.controller;

import de.edu.ltvt.dto.Question;
import de.edu.ltvt.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
/**
 * A controller providing the endpoints subsequent to /dashboard .
 */
@Controller
public class DashboardController {

    /**
     * The dashboard controller's question services.
     */
    private final QuestionService questionService;

    /**
     * A boolean to order all questions by date descendant
     */
    private boolean dateDesc = true;

    /**
     * A boolean to order all unaccepted questions by date descendant
     */
    private boolean dateDescUnaccepted = true;

    /**
     * A boolean to order all unanswered questions by date descendant
     */
    private boolean dateDescUnanswered = true;

    /**
     * A boolean to order the questions by the number of its answers descendant.
     */
    private boolean mostAnswered = true;

    /**
     * Creates a dashboard controller.
     * @param questionService The dashboard controller's question services.
     */
    @Autowired
    public DashboardController(QuestionService questionService) {
        this.questionService = questionService;
    }

    /**
     * A method to get the dashboard template.
     * @param model The actual model.
     * @return Teh dashboard template.
     */
    @GetMapping("/dashboard")
    public String dashboard (Model model) {
        List<Question> questions = questionService.getAll();
        questions.sort(Comparator.comparing(Question::getDate));
        Collections.reverse(questions);
        model.addAttribute("questions", questions);
        return "dashboard.html";
    }

    /**
     *  A method to sort the question by the number of its answers and to get the dashboard template.
     * @param model The actual model
     * @return The dashboard template.
     */
    @RequestMapping(value = "/dashboard", params = "answers", method = RequestMethod.POST)
    public String sortByAnswers (Model model) {
        List<Question> questions = questionService.getAll();
        questions.sort(Comparator.comparing(Question::getNumberOfAnswers));
        if(mostAnswered){
            mostAnswered = false;
        }else{
            Collections.reverse(questions);
            mostAnswered = true;
        }
        model.addAttribute("questions", questions);
        return "dashboard";
    }

    /**
     * A method to sort all question by its date descendant and to get the dashboard template.
     * @param model The actual model
     * @return The dashboard template.
     */
    @RequestMapping(value = "/dashboard", params = "date", method = RequestMethod.POST)
    public String sortByDate (Model model) {
        List<Question> questions = questionService.getAll();
        questions.sort(Comparator.comparing(Question::getDate));
        if(!dateDesc){
            Collections.reverse(questions);
            dateDesc = true;
        }else{
            dateDesc = false;
        }
        model.addAttribute("questions", questions);
        return "dashboard";
    }

    /**
     *  A method to sort unanswered question by its date descendant and to get the dashboard template.
     * @param model The actual model
     * @return The dashboard template.
     */
    @RequestMapping(value = "/dashboard", params = "unanswered", method = RequestMethod.POST)
    public String sortByUnanswered (Model model) {
        List<Question> questions = questionService.getAllUnanswered();
        questions.sort(Comparator.comparing(Question::getDate));
        if(!dateDescUnanswered){
            Collections.reverse(questions);
            dateDescUnanswered = true;
        }else{
            dateDescUnanswered = false;
        }
        model.addAttribute("questions", questions);

        return "dashboard";
    }

    /**
     *  A method to sort all not accepted question by its date and to get the dashboard template.
     * @param model The actual model
     * @return The dashboard template.
     */
    @RequestMapping(value = "/dashboard", params = "answered", method = RequestMethod.POST)
    public String sortByAnswered (Model model) {
        List<Question> questions = questionService.getAllNotAccepted();
        questions.sort(Comparator.comparing(Question::getDate));
        if(!dateDescUnaccepted){
            Collections.reverse(questions);
            dateDescUnaccepted = true;
        }else{
            dateDescUnaccepted = false;
        }
        model.addAttribute("questions", questions);
        return "dashboard";
    }

}
