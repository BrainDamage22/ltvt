package de.edu.ltvt.controller;

import de.edu.ltvt.dto.Answer;
import de.edu.ltvt.dto.Question;
import de.edu.ltvt.dto.User;
import de.edu.ltvt.services.AnswerService;
import de.edu.ltvt.services.QuestionService;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Iterator;
import java.util.List;

/**
 * A controller providing the endpoints subsequent to /displayQuestion .
 */
@Controller
public class DisplayQuestionController {

    /**
     * The displayQuestionController's question service.
     */
    private final QuestionService questionService;

    /**
     * The displayQuestionController's user service.
     */
    private final UserService userService;

    /**
     * The displayQuestionController's answer service.
     */
    private final AnswerService answerService;

    /**
     * Creates a display question controller.
     * @param questionService The displayQuestionController's question service.
     * @param userService The displayQuestionController's user service.
     * @param answerService The displayQuestionController's answer service.
     */
    @Autowired
    public DisplayQuestionController(QuestionService questionService, UserService userService, AnswerService answerService) {
        this.questionService = questionService;
        this.userService = userService;
        this.answerService = answerService;
    }

    /**
     * A method to get the display question template to display a question.
     * @param httpSession The actual session.
     * @param model The actual model.
     * @param questionId The id of the question.
     * @return The display question template.
     */
    @RequestMapping(value = "/displayQuestion", method = RequestMethod.GET)
    public String displayQuestion(HttpSession httpSession, Model model, @RequestParam("questionId") long questionId) {

        prepQuestionModel(model, questionId);

        return "displayQuestion";
    }

    /**
     * A method to answer a question and get the display question template.
     * @param session The actual session.
     * @param model The actual model.
     * @param u_answer The answer.
     * @param questionId The id of the question.
     * @return The display question template.
     */
    @RequestMapping(value = "/displayQuestion", params = "answerQuestion", method = RequestMethod.POST)
    public String answerQuestion(HttpSession session, Model model, @RequestParam("u_answer") String u_answer, @RequestParam("questionId") long questionId) {

        answerService.answerQuestion(new Answer(u_answer), questionId);

        prepQuestionModel(model, questionId);

        return "displayQuestion";
    }

    /**
     * A method to accept an answer an get the display question template.
     * @param session The actual session.
     * @param model The actual model.
     * @param questionId The id of the question.
     * @param answerId The id of the answer.
     * @return The display question template.
     */
    @RequestMapping(value = "displayQuestion", params = "acceptAnswer", method = RequestMethod.POST)
    public String acceptAnswer(HttpSession session, Model model, @RequestParam("acceptQuestionId") long questionId, @RequestParam("acceptAnswerId") long answerId) {
        answerService.acceptAnswer(answerId);
        prepQuestionModel(model, questionId);
        return "displayQuestion";
    }

    /**
     * A method to swap a bookmark and to get the display question template.
     * @param model The actual model.
     * @param questionId The question's id.
     * @return The display question template.
     */
    @RequestMapping(value = "/displayQuestion", params = "bookmark", method = RequestMethod.POST)
    public String swapBookmark(Model model, @RequestParam("bookmarkQuestionId") long questionId) {
        User currentUser = userService.getCurrentUser();
        Question question = questionService.findQuestionById(questionId);
        List<Question> bookmarks = currentUser.getBookmark();
        Iterator<Question> iterator = bookmarks.iterator();
        boolean found = false;

        while (iterator.hasNext()) {
            if (iterator.next().getId() == questionId) {
                found = true;
            }
        }

        if (found) {
            userService.removeQuestionfromBookmarks(currentUser, question);
        } else {
            userService.addQuestiontoBookmarks(currentUser, question);
        }

        prepQuestionModel(model, questionId);
        return "displayQuestion";
    }

    /**
     * A method to prepare the model.
     * @param model The actual model.
     * @param questionId The question id.
     */
    private void prepQuestionModel (Model model, long questionId) {
        Question question = questionService.findQuestionById(questionId);
        User questioner = userService.findUserById(question.getQuestioner().getUsername());
        List<Answer> answers = question.getAnswers();

        if (question.isAnswered()) {
            model.addAttribute("isAnswered", true);
            Answer acceptedAnswer = new Answer();
            for (Answer answer : answers) {
                if (answer.isAccepted()) {
                    acceptedAnswer = answer;
                }
            }
            model.addAttribute("acceptedAnswerId", acceptedAnswer.getId());
            model.addAttribute("acceptedAnswer", acceptedAnswer);
            Iterator<Answer> iterator = answers.iterator();
            while (iterator.hasNext()) {
                Answer it = iterator.next();
                if (acceptedAnswer.getId() == it.getId()) {
                    iterator.remove();
                }
            }
        } else {
            model.addAttribute("isAnswered", false);
        }
        if (userService.getCurrentUser() != null) {
            model.addAttribute("bookmark", userService.isQuestionBookmarked(userService.getCurrentUser(), question));
        }
        model.addAttribute("answers", answers);
        model.addAttribute("question", questionService.findQuestionById(questionId));
        model.addAttribute("questioner", questioner);
        model.addAttribute("nutzer", userService.getCurrentUserId());
    }


}
