package de.edu.ltvt.controller;

import de.edu.ltvt.dto.Question;
import de.edu.ltvt.services.QuestionService;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
/**
 * A controller providing the endpoints subsequent to /asknewquestion .
 */
@Controller
public class AskQuestionController {

    /**
     * The ask question controller's question service.
     */
    private final QuestionService questionService;

    /**
     * The ask question controller's user service.
     */
    private final UserService userService;

    /**
     * Creates an ask question controller.
     * @param questionService The ask question controller's question service.
     * @param userService The ask question controller's user service.
     */
    @Autowired
    public AskQuestionController(QuestionService questionService, UserService userService) {
        this.questionService = questionService;
        this.userService = userService;
    }



    @GetMapping("/asknewquestion")
    public String registrationForm(Model io_model) {
        io_model.addAttribute("question", new Question());
        return "askQuestionForm";
    }

    /**
     * A method to ask a new question and to get the ask question form template.
     * @param question The question to be asked.
     * @param bindingResult The binding results
     * @param model the actual model.
     * @return The ask new question form template.
     */
    @PostMapping("/asknewquestion")
    public ModelAndView sendMessage(@Valid Question question, BindingResult bindingResult, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        if (question.getTitle().isEmpty()) {
            modelAndView.setViewName("aksQuestionForm");
            return modelAndView;
        }

        if (question.getDescription().isEmpty()) {
            modelAndView.setViewName("aksQuestionForm");
            return modelAndView;
        }

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("aksQuestionForm");
            return modelAndView;
        }

        question.setQuestioner(userService.findUserById(userService.getCurrentUserId()));
        questionService.createQuestion(question);


        modelAndView.setViewName("redirect:displayQuestion");
        modelAndView.addObject("questionId", question.getId());

        return modelAndView;
    }
}


