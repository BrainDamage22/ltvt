package de.edu.ltvt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * A controller providing the endpoints subsequent to /login .
 */
@Controller
public class LoginController {

    /**
     * A method to get the login template.
     * @param io_model The actual model.
     * @return The login template.
     */
    @RequestMapping("/login")
    public String loginForm(Model io_model) {
        return "loginForm";
    }


}
