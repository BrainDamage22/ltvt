package de.edu.ltvt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * A controller providing the endpoints subsequent to /index , /contacts , /termsOfService , /legalNotices .
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String retIndex(Model io_model) {
        return "index.html";
    }

    /**
     * Anzeigen der Startseite
     *
     * @param io_model Aktuelles Model
     * @return Zielseite
     */
    @GetMapping("/index")
    public String index(Model io_model) {
        return "index.html";
    }

    /**
     * A method to get the contact template.
     * @param io_model The actuel model.
     * @return The contact template.
     */
    @GetMapping("/contacts")
    public String contacts(Model io_model) {
        return "contacts.html";
    }

    /**
     * A method to get the term of services template.
     * @param io_model The actuel model.
     * @return The term of services template.
     */
    @GetMapping("/termsOfService")
    public String termsOfService(Model io_model) { return "termsOfService.html"; }

    /**
     * A method to get the legal notice template.
     * @param io_model The actuel model.
     * @return The legal notice template.
     */
    @GetMapping("/legalNotices")
    public String legalNotices(Model io_model) { return "legalNotices.html"; }
}
