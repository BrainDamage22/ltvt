package de.edu.ltvt.controller;

import de.edu.ltvt.dto.Answer;
import de.edu.ltvt.dto.Question;
import de.edu.ltvt.services.AnswerService;
import de.edu.ltvt.services.QuestionService;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * A controller providing the endpoints subsequent to /answers .
 */
@Component
@RestController
@RequestMapping(value = "/answers")
public class AnswerController {

    /**
     * The answer controller's answer service.
     */
    private final AnswerService answerService;

    /**
     * The answer controller's question service.
     */
    private final QuestionService questionService;

    /**
     * The answer controller's user service.
     */
    private final UserService userService;

    /**
     * Creates an answer controller.
     * @param answerService The answer controller's answer service.
     * @param questionService The answer controller's question service.
     * @param userService The answer controller's user service.
     */
    @Autowired
    public AnswerController(AnswerService answerService, QuestionService questionService, UserService userService) {
        this.answerService = answerService;
        this.questionService = questionService;
        this.userService = userService;
    }

    /**
     * A method to answer a question and to get the index template.
     * @param io_model The acutal model.
     * @param u_answer The answer.
     * @param questionId The question id.
     * @return The index template.
     */
    @PostMapping("/answerQuestion")
    public String sendMessage(Model io_model, @RequestParam("u_answer") String u_answer,
                              @RequestParam("questionId") long questionId) {

        Answer answer = new Answer(u_answer);
        answerService.answerQuestion(answer, questionId);

        return "index";
    }
}
