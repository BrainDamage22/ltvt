package de.edu.ltvt.controller;

import de.edu.ltvt.dto.Answer;
import de.edu.ltvt.dto.Question;
import de.edu.ltvt.services.AnswerService;
import de.edu.ltvt.services.QuestionService;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DisplayUserBookmarksController {

    /**
     * The displayUserBookmarksController's answer service.
     */
    private final AnswerService answerService;

    /**
     * The displayUserBookmarksController's question service.
     */
    private final QuestionService questionService;

    /**
     * The displayUserBookmarksController's user service.
     */
    private final UserService userService;

    /**
     * Creates a displayUserBookmarksController.
     * @param answerService The displayUserBookmarksController's answer service
     * @param questionService The displayUserBookmarksController's question service.
     * @param userService The displayUserBookmarksController's user service.
     */
    @Autowired
    public DisplayUserBookmarksController(AnswerService answerService, QuestionService questionService, UserService userService) {
        this.answerService = answerService;
        this.questionService = questionService;
        this.userService = userService;
    }

    /**
     * A method to get the displayUserBookmarks template.
     * @param model The actual model.
     * @return The displayUserBookmarks template.
     */
    @GetMapping("displayUserBookmarks")
    public String displayUserBookmarks (Model model) {
        List<Question> bookmarks = userService.getCurrentUser().getBookmark();
        List<Question> removed = removeDuplicates(bookmarks);
        model.addAttribute("questions", removed);
        return "displayUserBookmarks";
    }

    /**
     * A method to remove duplicates from a list of questions.
     * @param list The list of questions to remove from.
     * @return The duplicate free list.
     */
    public static List<Question> removeDuplicates(List<Question> list)
    {
        List<Question> newList = new ArrayList<Question>();
        for (Question element : list) {
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }
        return newList;
    }

}
