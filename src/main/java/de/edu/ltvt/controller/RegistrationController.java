package de.edu.ltvt.controller;

import de.edu.ltvt.dto.User;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * A controller providing the endpoints subsequent to /register .
 */
@Controller
public class RegistrationController {

    /**
     * The registration conroller's user service.
     */
    private final UserService userService;

    /**
     * Creates a registration conroller.
     * @param userService The registration conroller's user service.
     */
    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    /**
     * An endpoint to provide the register page.
     * @param io_model The model.
     * @return the register from.
     */
    @GetMapping("/register")
    public String registrationForm(Model io_model) {
        io_model.addAttribute("user", new User());
        return "registrationForm";
    }

    /**
     * An endpoint to provide the register page.
     * @param checked
     * @param io_user The user.
     * @param io_bindingResult
     * @param io_model The model.
     * @param io_request The request.
     * @return The register template.
     */
    @PostMapping("/register")
    public String registerUser(@RequestParam(value = "checked", required = false) String checked, @Valid User io_user,
                               BindingResult io_bindingResult, Model io_model, WebRequest io_request)  {

        // Hier Fehler setzen damit diese immer in Verbindung mit allen anderen "darf
        // nicht leer sein"

        if (isPasswordEmpty(io_user.getPassword())) {
            io_model.addAttribute("invalidPassword", true);
            return "registrationForm";
        }

        // Passwort
        if (!isPasswordValid(io_user.getPassword())) {
            io_model.addAttribute("invalidPassword", true);
            return "registrationForm";
        }

        // Immer letzte Pruefung!!
        if (userService.isUserPresent(io_user.getUsername())) {
            io_model.addAttribute("userExists", true);
            return "registrationForm";
        }

        userService.createNutzer(io_user);
        return "loginForm";
    }

    /**
     * An endpoint to check if the password s empty.
     * @param iv_password The password.
     * @return @return {@code true} if not empty, {@code false} otherwise.
     */
    private boolean isPasswordEmpty(String iv_password) {
        return iv_password.trim().isEmpty();
    }

    /**
     * An endpoint to validate the password.
     * @param iv_password The password.
     * @return {@code true} if valid, {@code false} otherwise.
     */
    private boolean isPasswordValid(String iv_password) {
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,14}";
        if (!isPasswordEmpty(iv_password)) {
            return iv_password.matches(pattern);
        } else {
            return false;
        }
    }
}
