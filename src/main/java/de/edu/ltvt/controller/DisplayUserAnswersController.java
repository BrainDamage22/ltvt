package de.edu.ltvt.controller;

import de.edu.ltvt.dto.Answer;
import de.edu.ltvt.dto.Question;
import de.edu.ltvt.services.AnswerService;
import de.edu.ltvt.services.QuestionService;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * A controller providing the endpoints subsequent to /displayUserAnswers .
 */
@Controller
public class DisplayUserAnswersController {

    /**
     * The displayUserAnswersController's answers service.
     */
    private final AnswerService answerService;

    /**
     * The displayUserAnswersController's question service.
     */
    private final QuestionService questionService;

    /**
     * The displayUserAnswersController's user service.
     */
    private final UserService userService;

    /**
     * Creates a displayUserAnswersController.
     * @param answerService The displayUserAnswersController's answers service.
     * @param questionService The displayUserAnswersController's question service.
     * @param userService The displayUserAnswersController's user service.
     */
    @Autowired
    public DisplayUserAnswersController(AnswerService answerService, QuestionService questionService, UserService userService) {
        this.answerService = answerService;
        this.questionService = questionService;
        this.userService = userService;
    }

    /**
     * A method to get display user answer template.
     * @param model The actual model.
     * @return The display user answer template.
     */
    @GetMapping("displayUserAnswers")
    public String displayUserAnswers (Model model) {

        List<Answer> answers = answerService.getAllUserAnswers(userService.getCurrentUserId());
        List<Question> questions = new ArrayList<>();
        for (Answer answer : answers) {
            Question question = answer.getQuestion();
            questions.add(question);
        }

        List<Question> removedDuplicates = removeDuplicates(questions);
        model.addAttribute("questions", removedDuplicates);
        return "displayUserAnswers";
    }

    /**
     * A method to remove duplicates from a list of questions.
     * @param list The list of questions to remove from.
     * @return The duplicate free list.
     */
    public static List<Question> removeDuplicates(List<Question> list)
    {

        // Create a new ArrayList
        List<Question> newList = new ArrayList<Question>();

        // Traverse through the first list
        for (Question element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }

}
