package de.edu.ltvt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LtvtApplication extends SpringBootServletInitializer {

	/**
	 * The class of LtvtApplication.
	 */
	private static Class applicationClass = LtvtApplication.class;

	/**
	 * A method to start the application.
	 * @param args Additional parameters.
	 */
	public static void main(String[] args) {
		SpringApplication.run(LtvtApplication.class, args);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

}
