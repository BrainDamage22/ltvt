package de.edu.ltvt.dto;

import org.aspectj.weaver.patterns.TypePatternQuestions;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * An user in the LTVT-System.
 */
@Entity
public class User {

    /**
     * The username of the user.
     */
    @Id
    @Column(unique = true)
    @NotEmpty
    private String username;

    /**
     * The password of the user.
     */
    @NotEmpty
    private String password;

    /**
     * The list of roles the user has.
     */
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLES", joinColumns = {
            @JoinColumn(name = "USER_USERNAME", referencedColumnName = "username") }, inverseJoinColumns = {
            @JoinColumn(name = "ROLE_NAME", referencedColumnName = "name") })
    private List<Role> roles;

    /**
     * The list of questions questioned by the user.
     */
    @OneToMany(mappedBy = "questioner", fetch = FetchType.EAGER)
    private List<Question> questions;

    /**
     * The list of questions marked with a bookmark by the user.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Question> bookmark;

    /**
     * Creates a new user.
     */
    public User() {
    }

    /**
     * Creates a new user.
     * @param username The username of the user.
     * @param password The password of the user.
     */
    public User(@NotEmpty String username, @NotEmpty String password) {
        this.username = username;
        this.password = password;
    }

    // Getter & Setter
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Question> getBookmark() {
        return bookmark;
    }

    public void setBookmark(List<Question> bookmark) {
        this.bookmark = bookmark;
    }
}
