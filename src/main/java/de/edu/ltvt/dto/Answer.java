package de.edu.ltvt.dto;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * An answer in the LTVT-System.
 */
@Entity
public class Answer {

    /**
     * The id of the answer.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    /**
     * The content of the answer.
     */
    @NotNull
    private String content;

    /**
     * If the answer is accepted {@cod true} , otherwise {@code false}.
     */
    private boolean accepted;

    /**
     * The time when the answer was created.
     */
    @CreationTimestamp
    private Timestamp date;

    /**
     * The author of the answer.
     */
    @ManyToOne
    @JoinColumn(name = "fk_answerer")
    private User author;

    /**
     * The question, to which answ
     */
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    /**
     * Creates an answer.
     */
    public Answer (){}

    /**
     * Creates an answer.
     *
     * @param content The content of the answer.
     */
    public Answer(String content){
        this.content = content;
        this.accepted = false;
    }

    //Getter & Setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
