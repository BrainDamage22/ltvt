package de.edu.ltvt.dto;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * A question in the LTVT-System.
 */
@Entity
public class Question {

    /**
     * The id of the question.
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * The title of the question.
     */
    @NotEmpty
    private String title;

    /**
     * The description of the question.
     */
    @NotEmpty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    /**
     * The creation date of the question.
     */
    @CreationTimestamp
    private Timestamp date;

    /**
     * The author of the question.
     */
    @ManyToOne
    @JoinColumn(name = "fk_questioner")
    private User questioner;

    /**
     * A preview of the questions content.
     */
    private String substring;

    /**
     * if the question is answered {@code true}, otherwise {@code false}.
     */
    private boolean answered = false;

    /**
     * The list of answers for this question.
     */
    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
    private List<Answer> answers = new LinkedList<>();

    /**
     * The number of answers of this question.
     */
    private int numberOfAnswers;

    @ManyToMany
    private List<User> bookmarkers;

    /**
     * Creates a new question.
     */
    public Question() {
    }

    /**
     * Creates a new question.
     * @param title The title of the question.
     * @param description The description of the question.
     * @param date The date of the question.
     * @param questioner The questioner of the question.
     */
    public Question(@NotEmpty String title, @NotEmpty String description, Timestamp date, User questioner) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.questioner = questioner;
        this.numberOfAnswers = 0;
    }

    // Getter & Setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public User getQuestioner() {
        return questioner;
    }

    public void setQuestioner(User questioner) {
        this.questioner = questioner;
    }

    public boolean isAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getSubstring() {
        return substring;
    }

    public void setSubstring(String substring) {
        this.substring = substring;
    }

    public int getNumberOfAnswers() {
        return numberOfAnswers;
    }

    public void setNumberOfAnswers(int numberOfAnswer) {
        this.numberOfAnswers = numberOfAnswer;
    }
}
