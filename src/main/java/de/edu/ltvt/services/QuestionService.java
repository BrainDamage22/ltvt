package de.edu.ltvt.services;

import de.edu.ltvt.dto.Question;
import de.edu.ltvt.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
public class QuestionService {

    /**
     * The question service's question repository.
     */
    private final QuestionRepository questionRepository;

    /**
     * Creates a question service.
     * @param questionRepository
     */
    @Autowired
    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    /**
     * A method to create a new question.
     * @param question The question.
     */
    public void createQuestion(Question question) {
        String substring = question.getDescription();
        substring = substring.substring(0, Math.min(substring.length(), 197));
        substring = substring + "...";
        question.setSubstring(substring);
        questionRepository.save(question);
    }

    /**
     * A method to find a question by its id.
     * @param questionId The question's id.
     * @return The question or {@code null}
     */
    public Question findQuestionById (Long questionId) {
        return questionRepository.findById(questionId).orElse(null);
    }

    /**
     * A method to find all question, which are not accepted.
     * @return The list of unaccepted questions.
     */
    public List<Question> getAllNotAccepted(){return questionRepository.findAllNotAccepted();}
    /**
     * A method to find all question, which are not answered.
     * @return The list of unanswered questions.
     */
    public List<Question> getAllUnanswered(){
        List<Question> allUnanswered = new LinkedList<>();
        for(Question q : questionRepository.findAll()){
            if(q.getNumberOfAnswers() == 0)
            allUnanswered.add(q);
        }
        return  allUnanswered;
    }

    /**
     * A method to find all questions in the database.
     * @return The list of all questions.
     */
    public List<Question> getAll() {
        return questionRepository.findAll();
    }
}
