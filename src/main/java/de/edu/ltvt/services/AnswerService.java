package de.edu.ltvt.services;

import de.edu.ltvt.dto.Answer;
import de.edu.ltvt.dto.Question;
import de.edu.ltvt.dto.User;
import de.edu.ltvt.repository.AnswerRepository;
import de.edu.ltvt.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

/**
 * @author Vanessa
 */
@Service
public class AnswerService {


    /**
     * The answer repository for answers.
     */
    private final AnswerRepository answerRepository;

    /**
     * The question repository for answers.
     */
    private final QuestionRepository questionRepository;

    /**
     * The user service for answers.
     */
    private final UserService userService;

    /**
     * Creates a new answer service.
     * @param answerRepository The answer service's answer repository.
     * @param questionRepository The answer service's question repository.
     * @param userService The answer service's user service.
     */
    @Autowired
    public AnswerService(AnswerRepository answerRepository, QuestionRepository questionRepository,
                         UserService userService) {
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
        this.userService = userService;
    }


    /**
     * A method to set the answers author automatically and save it.
     * @param answer The Answer.
     * @param questionID The question id of the question to which the answer belong.
     */
    public void answerQuestion(Answer answer, Long questionID){
        answer.setAuthor((userService.findUserById(userService.getCurrentUserId())));
        saveAnswer(answer, questionID);
    }

    /**
     * A method to set the answers author automatically and save it.
     * Used to set authors for the answers in the databaseinitialiser.
     * @param answer The Answer.
     * @param questionID The question id of the question to which the answer belong.
     */
    public void answerQuestion(Answer answer, Long questionID, User user){
        answer.setAuthor(user);
        saveAnswer(answer, questionID);
    }

    /**
     * A method to save an connect an answer to its question.
     * @param answer The answer.
     * @param questionID he question id of the question to which the answer belong.
     */
    private void saveAnswer(Answer answer, Long questionID) {
        Question q = questionRepository.findByQuestionID(questionID);
        if (q != null) {
            Answer a = answerRepository.save(answer);
            a.setQuestion(q);
            List<Answer> answers = q.getAnswers();
            answers.add(a);
            q.setAnswers(answers);
            q.setNumberOfAnswers(q.getNumberOfAnswers()+1);
            answerRepository.save(a);
            questionRepository.save(q);
        }
    }

    /**
     * A method to accept an answer and therefore label the question as answered.
     * @param id The id of the answer.
     */
    public void acceptAnswer(Long id){
        Answer a = answerRepository.findByAnswerID(id);
        if( a != null && a.getQuestion().getQuestioner().getUsername().equals(userService.getCurrentUserId())){
        a.setAccepted(true);
        answerRepository.save(a);
        a.getQuestion().setAnswered(true);
        questionRepository.save(a.getQuestion());

        answerRepository.save(a);}
    }

    /**
     * A method to find all answers of an user.
     * @param username The name of the user.
     * @return The list of answers.
     */
    public List<Answer> getAllUserAnswers (String username) {
        List<Answer> allAnswers = answerRepository.findAll();

        allAnswers.removeIf(answer -> !answer.getAuthor().getUsername().equals(username));
        return allAnswers;
    }
}
