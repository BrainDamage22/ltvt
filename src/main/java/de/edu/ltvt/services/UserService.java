package de.edu.ltvt.services;

import de.edu.ltvt.dto.Question;
import de.edu.ltvt.dto.Role;
import de.edu.ltvt.dto.User;
import de.edu.ltvt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    /**
     * The user servise's user repository.
     */
    private final UserRepository userRepository;

    /**
     * Creates an user service.
     *
     * @param userRepository The user servise's user repository.
     */
    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * A method to create a new user.
     *
     * @param user The user to be created and safed.
     */
    public void createNutzer(User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        Role userRole = new Role("USER");
        List<Role> roles = new ArrayList<>();
        roles.add(userRole);
        user.setRoles(roles);
        userRepository.save(user);
    }

    /**
     * A method to get the current user's id.
     *
     * @return The id.
     */
    public String getCurrentUserId() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    /**
     * A method to check if a user exists.
     *
     * @param username The name of teh user.
     * @return {@code true} if it exists, {@false} otherwise.
     */
    public boolean isUserPresent(String username) {
        return findUserById(username) != null;
    }

    /**
     * A method to find a user in the database.
     *
     * @param username The username.
     * @return The user or {@code null}.
     */
    public User findUserById(String username) {
        return userRepository.findById(username).orElse(null);
    }

    /**
     * A method to get the current user from the database.
     *
     * @return The user or{@code null}
     */
    public User getCurrentUser() {
        return userRepository.findById(getCurrentUserId()).orElse(null);
    }

    /**
     * A method to get all questions od a user.
     *
     * @param username The username.
     * @return The list of questions.
     */
    public List<Question> getUserQuestions(String username) {
        List<Question> questions = getCurrentUser().getQuestions();


        return removeDuplicates(questions);
    }

    /**
     * A method to remove duplicates.
     *
     * @param list The list of questions to remove from.
     * @return The list of questions.
     */
    public static List<Question> removeDuplicates(List<Question> list) {
        List<Question> newList = new ArrayList<Question>();
        for (Question element : list) {
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }
        return newList;
    }

    /**
     * A method to add bookmarks to a user.
     *
     * @param user     The user of the bookmark.
     * @param question The question labeled with the bookmark.
     */
    public void addQuestiontoBookmarks(User user, Question question) {
        List<Question> bookmarks = user.getBookmark();
        bookmarks.add(question);
        user.setBookmark(bookmarks);
        userRepository.save(user);
    }


    /**
     * A method to remove bookmarks from a user.
     *
     * @param user     The user of the bookmark.
     * @param question The question labeled with the bookmark.
     */
    public void removeQuestionfromBookmarks(User user, Question question) {
        List<Question> bookmarks = user.getBookmark();
        bookmarks.removeIf(it -> it.getId() == question.getId());
        user.setBookmark(bookmarks);
        userRepository.save(user);
    }

    public boolean isQuestionBookmarked(User user, Question question) {
        List<Question> bookmarks = user.getBookmark();
        Iterator<Question> iterator = bookmarks.iterator();
        boolean found = false;
        while (iterator.hasNext()) {
            if (iterator.next().getId() == question.getId()) {
                found = true;
                break;
            }
        }
        return found;
    }
}
