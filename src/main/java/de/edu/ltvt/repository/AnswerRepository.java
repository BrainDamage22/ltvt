package de.edu.ltvt.repository;

import de.edu.ltvt.dto.Answer;
import de.edu.ltvt.dto.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * The database repository for answers.
 */
@Repository
public interface AnswerRepository extends JpaRepository<Answer,Long> {

    /**
     * A method to find an answer by its id.
     * @param id The id of the answer searched.
     * @return The answer or {@code null}
     */
    @Query("SELECT a FROM Answer AS a WHERE a.id = :id")
    Answer findByAnswerID(Long id);
}
