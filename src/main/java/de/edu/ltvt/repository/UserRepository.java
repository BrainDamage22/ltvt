package de.edu.ltvt.repository;

import de.edu.ltvt.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * The database repository for user.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

    /**
     * A method to find a user by its username.
     * @param name The name of the user.
     * @return The user or {@code null}.
     */
    @Query("SELECT u FROM User AS u WHERE u.username = :name")
            User findByUsername(@Param("name") String name);

}
