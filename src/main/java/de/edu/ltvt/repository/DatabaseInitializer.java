package de.edu.ltvt.repository;

import de.edu.ltvt.dto.Answer;
import de.edu.ltvt.dto.Question;
import de.edu.ltvt.dto.User;
import de.edu.ltvt.services.AnswerService;
import de.edu.ltvt.services.QuestionService;
import de.edu.ltvt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

/**
 * A class to initialise the database with test data.
 */
@Component
public class DatabaseInitializer implements ApplicationRunner {


    /**
     * The default password.
     */
    private static final String DEFAULT_PASSWORD = "Password1";

    /**
     * The title for dummy questions.
     */
    private static final String DEFAULT_QUESTION_TITLE = "Ich bin der Titel einer Frage";

    /**
     * The content of dummy  questions.
     */
    private static final String DEFAULT_QUESTION = "Lorem ipsum";

    /**
     * The content of dummy answers.
     */
    private static final String DEFAULT_ANSWER = "dolor sit amet";

    /**
     * The string to deaktivate the databaseinitialiser.
     */
    private static final String NO_INIT_ARGUMENT = "noDatabaseIntitialiser";

    /**
     * The user service for the databaseinitialiser.
     */
    private final UserService userService;

    /**
     * The user repository for the databaseinitialiser.
     */
    private final UserRepository userRepository;

    /**
     * The answer service for the databaseinitialiser.
     */
    private final AnswerService answerService;

    /**
     * The question service for the databaseinitialiser.
     */
    private final QuestionService questionService;

    /**
     * Creates a new database initializer.
     * @param userService The user service.
     * @param userRepository
     * @param answerService
     * @param questionService The question service.
     */
    @Autowired
    public DatabaseInitializer(UserService userService, UserRepository userRepository, AnswerService answerService, QuestionService questionService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.answerService = answerService;
        this.questionService = questionService;
    }

    /**
     * A method to store data in the database when starting the system.
     * @param args The command line arguments.
     */
    @Override
    @Transactional
    public void run(ApplicationArguments args) {

        if (!args.containsOption(NO_INIT_ARGUMENT)) {

            userService.createNutzer(new User("Michael",DEFAULT_PASSWORD));
            userService.createNutzer(new User("Matthias",DEFAULT_PASSWORD));
            userService.createNutzer(new User("Gordon",DEFAULT_PASSWORD));

            User michael = userRepository.findByUsername("Michael");
            User matthias = userRepository.findByUsername("Matthias");
            User gordon = userRepository.findByUsername("Gordon");


            questionService.createQuestion( new Question(DEFAULT_QUESTION_TITLE + " von Michael", DEFAULT_QUESTION,
                    new Timestamp(System.currentTimeMillis()),michael));
            questionService.createQuestion( new Question(DEFAULT_QUESTION_TITLE + " von Matthias", DEFAULT_QUESTION,
                    new Timestamp(System.currentTimeMillis()),matthias));

            answerService.answerQuestion(new Answer(DEFAULT_ANSWER),2L, gordon);
            answerService.answerQuestion(new Answer(DEFAULT_ANSWER),2L, michael);
            answerService.answerQuestion(new Answer(DEFAULT_ANSWER),2L, gordon);

        }
    }
}

