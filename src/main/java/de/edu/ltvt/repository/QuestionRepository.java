package de.edu.ltvt.repository;

import de.edu.ltvt.dto.Question;
import de.edu.ltvt.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The database repository for aquestion.
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

        /**
         * A method to find a question by its id.
         * @param id The question's id.
         * @return The question or {@code null}
         */
        @Query("SELECT q FROM Question AS q WHERE q.id = :id")
        Question findByQuestionID(@Param("id") Long id);

        /**
         * A method to find all question, which are not accepted.
         * @return The list of unaceepted questions.
         */
        @Query("SELECT q FROM Question AS q WHERE q.answered = false ")
        List<Question> findAllNotAccepted();


}
