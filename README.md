# LTVT

## Starten der Anwendung
LTVT ist ein vereinfachtes Question&Answering-System in dem Fragen gestellt und beantwortet werden können.
Vorraussetzung für diese Anwendung ist min. Java 1.8.
Aus dem `target` - Verzeichnis des Projektes kann über die Kommandozeile ( mit folgendem Befehl)
 die Anwendung LTVT gestartet werden:

`java -jar LTVT-0.0.1-SNAPSHOT.jar`

Durch Hinzufügen von `--noDatabaseIntitialiser` wird zusätzlich der standartmäßig aktivierte Datenbankinitialiser, mit den 
vordefinierten Benutzer, Fragen und Antworten, deaktiviert.
Nachdem das System gestartet ist, kann man unter `http://localhost:8080` auf die Startseite der Anwendung zugreifen.


## Benutzung der Anwendung
### Registrierung, Login, Logout
In dem System kann man sich über den Button `Registrieren` registrieren. Hierfür muss dann ein Benutzername(darf nicht schon im System sein, mind. 5 
Zeichen) und ein Passwort (8-14 Zeichen lang,mind. ein Groß- und Kleinbuchstabe und eine Zahl) angegeben werden.
Nach erfolgereicher Registrierung wird man direkt auf die Loginseite weitergeleitet und muss sich hier mit seinen registrieten Daten anmelden.
Ist man schon registriert kann man sich über den `Login` Button einloggen.
Um sich aus dem System wieder abzumelden klickt man auf den Button `Menü` und danach auf `Logout`.

### Fragen durchsehen
Auf der Startseite kann man über den Button `Browse Questions` die Fragen des Systems einsehen. Diese Fragen können 
über den entsprechenden Button nach allen Fragen/ unbeantworteten Fragen / nicht gelösten Fragen gefiltert werden.
Klickt man auf eine der Fragen, wird diese vollständig mit all ihren Antworten dargestellt. Als eingeloggter Benutzer kann 
man diese Frage auch beantworten.

### Frage stellen
Als eingeloggter Benutzer kann auf der Startseite über den Button `Frage stellen`  eine Frage gestellt werden. Hier muss ein Titel und eine Beschreibung angegeben werden.
Beides ist auf 255 Zeichen beschränkt. Danach wird die entsprechende Frage angezeigt. 

### Menü Button
Als eingeloggter Benutzer steht einem ein `Menü` Button zur Verfügung. Die Optionen `Frage stellen` und `Logout`wurden schon erklärt.
Unter der Option `gestellte Fragen` kann der Benutzer die von sich gestellen Fragen einsehen.
Der Benutzer hat auch die Möglichkeit eine der Antworten zu einer jeweiligen Frage zu akzeptieren und damit gilt die Frage als gelöst.
Unter der Option `meine Antworten` kann der Benutzer alle seine geschriebenen Antworten einsehen.


## Besonderheiten

### Design
Für eine optimale Nutzung der Anwedung wurde diese für diverse Engeräte responsiv designed.

### Datenbankinitialisierung
Ist der Databaseinitialiser aktiviert, so sind bei Start des Systems folgende Objekte bereits im System:

0. 3 Benutzer: Michael, Matthias, Gordon.
    Das Passwort ist bei allen: `Password1`
0. 2 Fragen: Eine von Michael und eine von Matthias gestellt.
0. 3 Antworten: 2 von Gordon, 1 von Michael


## Teammitglieder
Vanessa Hermann (78317)
Thomas Jonas (82060)
Lukas Altmann (79717)